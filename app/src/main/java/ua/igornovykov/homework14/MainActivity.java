package ua.igornovykov.homework14;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView alevel;
    ImageView imageAlevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alevel = findViewById(R.id.alevel);
        imageAlevel = findViewById(R.id.image_alevel);
        TextView teacherAlevel = new TextView(this);
        LinearLayout  ml = findViewById(R.id.mainLayout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);

        lp.weight = 0.2f;
        teacherAlevel.setText("Our teacher: Vitaliy Ptitsyn");
        teacherAlevel.setTextColor(Color.MAGENTA);
        teacherAlevel.setTextSize(25);
        teacherAlevel.setLayoutParams(lp);
        ml.addView(teacherAlevel,lp);

        ImageView vp = new ImageView(this);
        lp.weight = 0.8f;
        vp.setImageResource(R.drawable.vitaliy_ptitsyn);
        vp.setLayoutParams(lp);
        ml.addView(vp,lp);
    }
}
